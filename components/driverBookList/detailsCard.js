import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, map,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './style';

class DetailsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            spinner: false,
            driverName: '',
            driverCode: ''
        }
    }
    componentDidMount() {
        if (this.props.driverList.trip_status == 0) {
            this.driverName(this.props.driverList.driver_id)
        }
        else {
            this.driverName(this.props.driverList.trip_driver)
        }
    }
    // this.props.route.params.data.trip_driver

    driverName(driverId) {
        console.log(driverId, 'driverId one')
        fetch('https://keralawings.co.in/booking/api/driver_name', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'driver_id=' + driverId
        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson, 'driverId driverId driverName');
                if (responseJson.status == true) {
                    this.setState({
                        driverName: responseJson.d_name[0].first_name,
                        driverCode: responseJson.d_name[0].code,
                    })
                }
                else {
                    driverName: ''
                    driverCode: ''
                }
            }).catch((error) => {
                console.warn(error, 'error');
            });
    }

    render() {
        return (
            <View style={{ marginTop: -20 }}>
                <View style={styles.loginView}>
                    {this.props.driverList.id == this.props.id &&
                        <View style={styles.cardView}>
                            <View style={styles.secondCardView}>
                                <View style={{ width: '100%' }}>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Customer Name</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.customer_name} </Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Customer Number</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.customer_number}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Landnumber</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.landnumber}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Address</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.address}</Text>
                                    </View>

                                    {this.props.driverList.trip_status == 2 &&
                                        <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                            <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Amount</Text>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                            <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.amount} </Text>
                                        </View>
                                    }

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Location</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.location}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Destination</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.destination}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Booking Date</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.booking_date} </Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>End Date</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.end_date}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Time</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.timepicker}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Number of Days</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.no_day}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Remarks</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.remarks} </Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Status</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        {this.props.driverList.status == 1 &&
                                            <Text style={{ marginLeft: 10, fontSize: 14, color: "#02bcb1" }}>Active</Text>
                                        }
                                        {this.props.driverList.status == 2 &&
                                            <Text style={{ marginLeft: 10, fontSize: 14, color: "red" }}>Closed</Text>
                                        }
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Trip Status</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        {this.props.driverList.trip_status == 0 &&
                                            <Text style={{ marginLeft: 10, fontSize: 14 }}>Not yet start</Text>
                                        }
                                        {this.props.driverList.trip_status == 1 &&
                                            <Text style={{ marginLeft: 10, fontSize: 14, color: "#02bcb1" }}>Currently Running</Text>
                                        }
                                        {this.props.driverList.trip_status == 2 &&
                                            <Text style={{ marginLeft: 10, fontSize: 14, color: "red" }}>Closed</Text>
                                        }
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Driver name and code</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.state.driverName}  {this.state.driverCode}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.vehicle}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Name</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}> {this.props.driverList.vehicle_name} </Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Number</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.vehicle_number}</Text>
                                    </View>

                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Type</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                        <Text style={{ marginLeft: 10, fontSize: 14 }}>{this.props.driverList.vehicle_type}</Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    }
                </View>
            </View>
        )
    }
}
export default DetailsCard;
