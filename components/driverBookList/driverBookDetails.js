import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, map, BackHandler, 
    StatusBar,
    Image, ActivityIndicator, FlatList,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './style';
import DetailsCard from './detailsCard';

class DriverBookDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            spinner: false,
            driverName: ''
        }
    }
   
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }
    
    driverName(driverId) {
        fetch('https://keralawings.co.in/booking/api/driver_name', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'driver_id=' + driverId
        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson, 'driverId driverId ');
                // if (responseJson.status == true) {
                //     this.setState({
                //         spinner: false,
                //         // driverName: 
                //     })
                // }
                // else {
                //     this.setState({
                //         spinner: false,
                //     })
                // }


            }).catch((error) => {
                console.warn(error, 'error');
            });
    }

    render() {
        return (
            <View style={{ backgroundColor: '#f7f7f7' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :
                    <View>
                        <View style={styles.mainContainer}>
                            <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>DRIVER BOOKING DETAILS</Text>
                        </View>
                        <View style={styles.subContainer}>
                            <View style={styles.loginView}>
                                <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 10, paddingTop: 20, position: 'relative', zIndex: -1, paddingBottom: 225 }}>
                                  
                                    <FlatList
                                        data={this.props.route.params.data}
                                        keyExtractor={(item, index) => index.toString()}
                                        // numColumns={2}
                                        renderItem={({ item }) => <DetailsCard driverList={item} id={this.props.route.params.id}
                                        />}
                                    />
                                    {/* {this.props.route.params.data.map((item, index) =>
                                        <View onPress={this.assetDetails} onPress={this.bookingDetails} style={styles.cardView}>
                                            <View style={styles.secondCardView}>
                                                <View style={{ width: '100%' }}>
                                                    {item.trip_status == 0 ?
                                                        this.driverName(item.driver_id)
                                                        :
                                                        this.driverName(item.trip_driver)
                                                    }
                                                   
                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Customer Name</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.customer_name} </Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Customer Number</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.customer_number}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Landnumber</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.landnumber}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Address</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.address}</Text>
                                                    </View>

                                                    {item.trip_status == 2 &&
                                                        <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                            <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Amount</Text>
                                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                            <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.amount} </Text>
                                                        </View>
                                                    }

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Location</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.location}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Destination</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.destination}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Booking Date</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.booking_date} </Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>End Date</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.end_date}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Time</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.timepicker}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Number of Days</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.no_day}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Remarks</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.remarks} </Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Status</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        {item.status == 1 &&
                                                            <Text style={{ marginLeft: 10, fontSize: 15, color: "#02bcb1" }}>Active</Text>
                                                        }
                                                        {item.status == 2 &&
                                                            <Text style={{ marginLeft: 10, fontSize: 15, color: "red" }}>Closed</Text>
                                                        }
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Trip Status</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        {item.trip_status == 0 &&
                                                            <Text style={{ marginLeft: 10, fontSize: 15 }}>Not yet start</Text>
                                                        }
                                                        {item.trip_status == 1 &&
                                                            <Text style={{ marginLeft: 10, fontSize: 15, color: "#02bcb1" }}>Currently Running</Text>
                                                        }
                                                        {item.trip_status == 2 &&
                                                            <Text style={{ marginLeft: 10, fontSize: 15, color: "red" }}>Closed</Text>
                                                        }
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Trip Driver</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.trip_driver}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.vehicle}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Name</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.vehicle_name} </Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Number</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.vehicle_number}</Text>
                                                    </View>

                                                    <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                        <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Vehicle Type</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                        <Text style={{ marginLeft: 10, fontSize: 15 }}>{item.vehicle_type}</Text>
                                                    </View>
                                                </View>
                                            </View>

                                        </View>
                                    )} */}
                                </ScrollView>
                            </View>
                        </View>
                    </View>

                }
            </View>

        )
    }
}
export default DriverBookDetails;
