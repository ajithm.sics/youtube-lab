import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, Alert,
    StatusBar, Picker,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './registerStyle';
import mainStyles from '../mainStyle';
// import { createStackNavigator } from '@react-navigation/stack';
// import { NavigationContainer } from '@react-navigation/native';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowPassword: true,
            error: false,
            spinner: false,
            id: '',
            name: '',
            lastName: '',
            email: '',
            phone: '',
            landPhone: '',
            vehicleName: '',
            vehicleType: '',
            vehicleNumber: '',
            address: '',
            password: '',
            location: '',
            confirmPassword: '',
            message: '',
            showVehicleButtons: true,
            driverFields: false,
            vehicletransmission: ''
        }
    }

    componentDidMount() {
        var ref = this;
        AsyncStorage.getItem('userdetails', async (err, result) => {
            var res = await JSON.parse(result);
            console.log(res, 'state responseJson userdetails service engin')
            this.setState({
                name: res.name,
                lastName: res.lastname,
                phone: res.phone,
                landPhone: res.landPhone,
                vehicleName: res.vehicleName,
                vehicleType: res.vehicleType,
                vehicleNumber: res.vehicleNumber,
                address: res.address,
                id: res.id
            })
        });
    }

    // driver
    clickRegister = () => {
        console.log(this.state.id, "hello id")
        console.log(this.state.phone, "hello phone")
        console.log(this.state.name, "hello name")
        console.log(this.state.address, "hello address")
        console.log(this.state.location, "hello location")
        console.log(this.state.landPhone, "hello landPhone")
        console.log(this.state.vehicleName, "hello vehicleName")
        console.log(this.state.vehicleType, "hello vehicleType")
        console.log(this.state.vehicleNumber, "hello vehicleNumber")
        console.log(this.state.vehicletransmission, "hello vehicletransmission")

        this.setState({ spinner: true, error: true })
        if (this.state.name == "" || this.state.name == null || this.state.phone == "" || this.state.phone.length < 10 ||
            this.state.vehicleName == "" || this.state.vehicleName == null || this.state.vehicleType == "" ||
            this.state.location == "" || this.state.location == null ||
            this.state.vehicleType == null || this.state.vehicleNumber == "" || this.state.vehicleNumber == null ||
            this.state.address == "" || this.state.address == null || this.state.landPhone == "" ||
            this.state.landPhone == null || this.state.vehicletransmission == null || this.state.vehicletransmission == "") {
            ToastAndroid.showWithGravity(
                'Please fill the mandatory Fields',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
            this.setState({ spinner: false })
        }
        else {
            fetch('https://keralawings.co.in/booking/api/profile_update_driver', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'user_id=' + this.state.id + '&name=' + this.state.name + '&phone=' + this.state.phone
                    + '&address=' + this.state.address + '&location=' + this.state.location + '&vehicleName=' + this.state.vehicleName
                    + '&vehicleType=' + this.state.vehicleType + '&vehicleNumber=' + this.state.vehicleNumber + '&landnumber=' + this.state.landPhone
                    + '&vehicletransmission=' + this.state.vehicletransmission

            }).then((response) => response.json())
                .then(async (responseJson) => {
                    console.log(responseJson, 'hello state res ');
                    if (responseJson.status == true) {
                        ToastAndroid.showWithGravity(
                            'Profile updated Successfully',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        this.setState({
                            spinner: false
                        })
                        this.props.navigation.navigate('Bhome')
                        AsyncStorage.setItem("userdetails", JSON.stringify(this.state));
                    }
                    else {
                        ToastAndroid.showWithGravity(
                            'Profile Error',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        alert(responseJson.msg)
                        this.setState({
                            spinner: false,
                        })
                    }


                }).catch((error) => {
                    console.warn(error, 'error');
                });
        }

    }

    showPassword = () => {
        this.setState({
            ShowPassword: !this.state.ShowPassword
        });
    }

    /* <View style={mainStyles.loader}>
        <Image
            source={require('../../assets/icons/loader.gif')}
            style={{ width: 50, height: 50 }} />
       
        <Text style={mainStyles.spinnerTextStyle}>please wait</Text>
    </View> */
    clickLogin = () => {
        this.props.navigation.navigate('Login')
    }

    clickDriver = () => {
        this.setState({
            showVehicleButtons: false,
            driverFields: true
        })
    }

    // taxi
    clickCustomer = () => {

        this.setState({
            showVehicleButtons: false,
            spinner: true
        })

        if (this.state.name == "" || this.state.name == null || this.state.phone == "" || this.state.phone.length < 10 ||
            this.state.address == "" || this.state.address == null ||
            this.state.location == "" || this.state.location == null ||
            this.state.landPhone == null || this.state.landPhone == "") {
            ToastAndroid.showWithGravity(
                'Please fill the mandatory Fields',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
            this.setState({ spinner: false })
        }
        else {
            fetch('https://keralawings.co.in/booking/api/profile_update_taxi', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'user_id=' + this.state.id + '&phone=' + this.state.phone + '&name=' + this.state.name
                    + '&address=' + this.state.address + '&location=' + this.state.location + '&landnumber=' + this.state.landPhone

            }).then((response) => response.json())
                .then(async (responseJson) => {
                    console.log(responseJson, 'hello state res ');
                    if (responseJson.status == true) {
                        ToastAndroid.showWithGravity(
                            'Profile updated Successfully',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        this.setState({
                            spinner: false
                        })
                        this.props.navigation.navigate('Bhome')
                        AsyncStorage.setItem("userdetails", JSON.stringify(this.state));
                    }
                    else {
                        ToastAndroid.showWithGravity(
                            'Profile Error',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        alert(responseJson.msg)
                        this.setState({
                            spinner: false,
                        })
                    }


                }).catch((error) => {
                    console.warn(error, 'error');
                });
        }
    }

    clickReset = () => {
        this.setState({
            showVehicleButtons: true,
            driverFields: false
        })
    }

    render() {
        return (
            <View style={{ backgroundColor: '#f5f5f5' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :

                    <View style={{ width: '100%', backgroundColor: '#ffffff' }}>
                        <View style={styles.loginView}>
                            <ScrollView contentContainerStyle={{ flexGrow: 1, position: 'relative', zIndex: -1, paddingBottom: 25 }}>
                                {/* <View style={{ alignItems: 'center', marginTop: 30 }}>
                                    <Image
                                        source={require('../../assets/images/icon.png')}
                                        style={{ width: 100, height: 100 }} />
                                </View> */}
                                <View style={styles.mainContainer}>
                                    <Text style={{ letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>PROFILE REGISTER</Text>
                                </View>

                                <View style={{ paddingHorizontal: 20 }}>
                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Phone"
                                            keyboardType='numeric'
                                            placeholderTextColor="#202020"
                                            value={this.state.phone}
                                            onChangeText={(phoneString) => { this.setState({ phone: phoneString }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.phone == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the phone number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }
                                    {this.state.error &&
                                        <View>
                                            {this.state.phone !== "" && this.state.phone.length < 10 ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Enter the 10 digit phone number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Name"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.name}
                                            onChangeText={(nameString) => { this.setState({ name: nameString }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {(this.state.name == "" || this.state.name == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Name</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Address"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.address}
                                            onChangeText={(addressString) => { this.setState({ address: addressString }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {(this.state.address == "" || this.state.address == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the address</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Location"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.location}
                                            onChangeText={(locationString) => { this.setState({ location: locationString }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {(this.state.location == "" || this.state.location == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Location</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    {/* <View style={styles.inputView}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="Last Name"
                                        placeholderTextColor="#919e9a"
                                        value={this.state.lastName}
                                        onChangeText={(lastNameString) => { this.setState({ lastName: lastNameString }) }}
                                    />
                                </View> */}

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Land Phone"
                                            keyboardType='numeric'
                                            placeholderTextColor="#919e9a"
                                            value={this.state.landPhone}
                                            onChangeText={(landPhoneString) => { this.setState({ landPhone: landPhoneString }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.landPhone == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the landphone number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    {this.state.driverFields == true &&
                                        <View>
                                            <View style={styles.inputView}>
                                                <TextInput
                                                    style={styles.input}
                                                    placeholder="Vehicle Name"
                                                    placeholderTextColor="#919e9a"
                                                    value={this.state.vehicleName}
                                                    onChangeText={(vehicleNameString) => { this.setState({ vehicleName: vehicleNameString }) }}
                                                />
                                            </View>
                                            {this.state.error &&
                                                <View>
                                                    {(this.state.vehicleName == "" || this.state.vehicleName == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle name</Text> :
                                                        <Text style={{ display: 'none' }}></Text>
                                                    }
                                                </View>
                                            }

                                            <View style={styles.inputView}>
                                                <TextInput
                                                    style={styles.input}
                                                    placeholder="Vehicle Type"
                                                    placeholderTextColor="#919e9a"
                                                    value={this.state.vehicleType}
                                                    onChangeText={(vehicleTypeString) => { this.setState({ vehicleType: vehicleTypeString }) }}
                                                />
                                            </View>
                                            {this.state.error &&
                                                <View>
                                                    {(this.state.vehicleType == "" || this.state.vehicleType == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle type</Text> :
                                                        <Text style={{ display: 'none' }}></Text>
                                                    }
                                                </View>
                                            }

                                            <View style={styles.inputView}>
                                                <TextInput
                                                    style={styles.input}
                                                    placeholder="Vehicle Number"
                                                    placeholderTextColor="#919e9a"
                                                    value={this.state.vehicleNumber}
                                                    onChangeText={(vehicleNumberString) => { this.setState({ vehicleNumber: vehicleNumberString }) }}
                                                />
                                            </View>
                                            {this.state.error &&
                                                <View>
                                                    {(this.state.vehicleNumber == "" || this.state.vehicleNumber == null) ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle number</Text> :
                                                        <Text style={{ display: 'none' }}></Text>
                                                    }
                                                </View>
                                            }

                                            <View style={styles.inputView}>
                                                <Picker selectedValue={this.state.vehicletransmission} style={styles.input} onValueChange={(value) => { this.setState({ vehicletransmission: value }) }}>
                                                    <Picker.Item label="Automatic" value="Automatic" />
                                                    <Picker.Item label="Mannual" value="Mannual" />
                                                </Picker>
                                            </View>
                                            {this.state.error &&
                                                <View>
                                                    {this.state.vehicletransmission == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Vehicle transmission</Text> :
                                                        <Text style={{ display: 'none' }}></Text>
                                                    }
                                                </View>
                                            }



                                        </View>
                                    }
                                    {this.state.showVehicleButtons == true &&
                                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TouchableOpacity onPress={this.clickCustomer} style={styles.customerButton}>
                                                <Text style={{ color: '#ffffff', fontSize: 14, letterSpacing: 0.5 }}>CUSTOMER</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={this.clickDriver} style={styles.driverButton}>
                                                <Text style={{ color: '#ffffff', fontSize: 14, letterSpacing: 0.5 }}>DRIVER</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }

                                    {this.state.showVehicleButtons == false &&
                                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TouchableOpacity onPress={this.clickRegister} style={styles.updateButton}>
                                                <Text style={{ color: '#ffffff', fontSize: 14, letterSpacing: 0.5 }}>REGISTER</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={this.clickReset} style={styles.resetButton}>
                                                <Text style={{ color: '#ffffff', fontSize: 14, letterSpacing: 0.5 }}>RESET</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </View>
                            </ScrollView>
                        </View>

                    </View>
                }
            </View>
        )
    }
}
export default Register;
