import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity, BackHandler,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';


// import styles from './homeStyle';
// import mainStyles from '../mainStyle';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';


import DateTimePicker from '@react-native-community/datetimepicker';

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Password: '',

            date: new Date(1598051730000),
            mode: 'time',
            show: false
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }
    render() {
        const { selectedHours, selectedMinutes } = this.state;
        return (
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#02bcb1', fontWeight: 'bold', fontSize: 24, letterSpacing: 1 }}>SETTINGS</Text>
            </View>
        )
    }
}
export default Settings;