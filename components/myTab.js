import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    ToastAndroid,
} from 'react-native';
import Login from './Login/login';
import Register from './register/register';
import Home from './home/home';
import DriverBooking from './driverBooking/driverBooking';
import TaxiBooking from './taxiBooking/index';
import TaxiBookList from './taxiBookList/index';
import DriverBookList from './driverBookList/index';
import DriverBookDetails from './driverBookList/driverBookDetails';
import TaxiBookDetails from './taxiBookList/taxiBookDetails';
import mainStyles from './mainStyle';
import Profile from './profile/profile';
import BookingList from './bookingList/bookingList';
// import Chat from './chat/chat';
import Settings from './settings/settings';
import Logout from './logout/logout';
import OTP from './otp/otp';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function HomeStackScreen() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
            <Stack.Screen name="DriverBooking" component={DriverBooking} options={{ headerShown: false }} />
            <Stack.Screen name="TaxiBooking" component={TaxiBooking} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

function BookingListStackScreen() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="BookingList" component={BookingList} options={{ headerShown: false }} />
            <Stack.Screen name="TaxiBookList" component={TaxiBookList} options={{ headerShown: false }} />
            <Stack.Screen name="DriverBookList" component={DriverBookList} options={{ headerShown: false }} />
            <Stack.Screen name="DriverBookDetails" component={DriverBookDetails} options={{ headerShown: false }} />
            <Stack.Screen name="TaxiBookDetails" component={TaxiBookDetails} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

function ProfileStackScreen() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

function LogoutStackScreen() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Logout" component={Logout} />
            {/* other screens */}
        </Stack.Navigator>
    );
}

function HomeStack() {
    return (
        <Tab.Navigator tabBarOptions={{
            style: {
                // paddingVertical: 20,
                backgroundColor: '#dee0e0'
            }
        }}>

            <Tab.Screen name="HomeStackScreen" component={HomeStackScreen}

                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused, color }) => (
                        focused ?
                            <Image
                                source={require('../assets/images/homeactive.png')}
                                style={mainStyles.activeTab} />
                            :
                            <Image
                                source={require('../assets/images/home.png')}
                                style={{ width: 23, height: 23, resizeMode: "stretch" }} />
                    ),
                }}
            />

            <Tab.Screen name="BookingListStackScreen" component={BookingListStackScreen}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused, red }) => (
                        focused ?
                            <Image
                                source={require('../assets/images/chatactive.png')}
                                style={mainStyles.activeTab} />
                            :
                            <Image
                                source={require('../assets/images/chat.png')}
                                style={{ width: 22, height: 22 }} />
                    ),
                }} />

            <Tab.Screen name="ProfileStackScreen" component={ProfileStackScreen}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused, color }) => (
                        focused ?
                            <Image
                                source={require('../assets/images/accountactive.png')}
                                style={mainStyles.activeTab} />
                            :
                            <Image
                                source={require('../assets/images/account.png')}
                                style={{ width: 20, height: 22 }} />
                    ),
                }} />

            <Tab.Screen name="LogoutStackScreen" component={LogoutStackScreen}
                options={{
                    //  style={ backgroundColor: 'green'},
                    tabBarLabel: '',
                    tabBarIcon: ({ focused, red }) => (
                        focused ?
                            <Image
                                source={require('../assets/images/logout.png')}
                                style={mainStyles.activeTab} />
                            :
                            <Image
                                source={require('../assets/images/logout.png')}
                                style={{ width: 22, height: 23 }} />
                    ),
                }} />
        </Tab.Navigator>
    );
}

class Mytab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            connection_Status: "",
        }
    }


    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                    <Stack.Screen name="OTP" component={OTP} options={{ headerShown: false }} />
                    <Stack.Screen name="Bhome" component={HomeStack} options={{ headerShown: false }} />

                    {/* <Stack.Screen name="DriverBooking" component={DriverBooking} options={{ headerShown: false }} /> */}
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Mytab;