import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, BackHandler, 
    Text, map,
    StatusBar,
    Image, ActivityIndicator, FlatList,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './style';
import DetailsCard from './detailsCard';

class TaxiBookDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            spinner: false,
            driverName: ''
        }
    }
    
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }
    
    driverName(driverId) {
        fetch('https://keralawings.co.in/booking/api/driver_name', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'driver_id=' + driverId
        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson, 'driverId driverId ');
            }).catch((error) => {
                console.warn(error, 'error');
            });
    }

    render() {
        return (
            <View style={{ backgroundColor: '#f7f7f7' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :
                    <View>
                        <View style={styles.mainContainer}>
                            <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>TAXI BOOKING DETAILS</Text>
                        </View>
                        <View style={styles.subContainer}>
                            <View style={styles.loginView}>
                                <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 10, paddingTop: 20, position: 'relative', zIndex: -1, paddingBottom: 225 }}>
                                    <FlatList
                                        data={this.props.route.params.data}
                                        keyExtractor={(item, index) => index.toString()}
                                        // numColumns={2}
                                        renderItem={({ item }) => <DetailsCard driverList={item} id={this.props.route.params.id}
                                        />}
                                    />
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                }
            </View>

        )
    }
}
export default TaxiBookDetails;
