import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, BackHandler,
    Text, map,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './style';

class TaxiBookList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            firstName: '',
            phone: '',
            landnumber: '',
            address: '',
            vehicleName: '',
            vehicleType: '',
            vehicleNumber: '',
            spinner: true,
            length: 0,
            bookList: []
        }
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    componentDidMount() {
        var ref = this;
        AsyncStorage.getItem('userdetails', async (err, result) => {
            var res = await JSON.parse(result);
            console.log(res, 'responseJson userdetails service engin')
            this.setState({
                userId: res.id,
                firstName: res.name,
                phone: res.phone,
                landnumber: res.landPhone,
                address: res.address,
                vehicleName: res.vehicleName,
                vehicleType: res.vehicleType,
                vehicleNumber: res.vehicleNumber,
            })
            this.getDriverBookList(res.phone)
        });
    }

    getDriverBookList = (phone) => {
        console.log(phone, 'mobile')
        this.setState({ spinner: true })
        fetch('https://keralawings.co.in/booking/api/list_all_booking_taxi', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'customer_number=' + phone
        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson, 'mobile state res ');
                if (responseJson.status == true) {
                    this.setState({
                        spinner: false,
                        bookList: responseJson.all_taxibooking,
                        length: responseJson.all_taxibooking.length
                    })
                }
                else {
                    this.setState({
                        spinner: false,
                        length: responseJson.all_taxibooking.length
                    })
                }


            }).catch((error) => {
                console.warn(error, 'error');
            });
    }

    bookingDetails = (id) => {
        this.props.navigation.navigate("TaxiBookDetails", {data: this.state.bookList, id: id })
    }

    render() {
        return (
            <View style={{ backgroundColor: '#f7f7f7' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :

                    <View>
                        <View style={styles.mainContainer}>
                            <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>TAXI BOOKING LIST</Text>
                        </View>
                        <View style={styles.subContainer}>
                            <View style={styles.loginView}>
                                <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 10, paddingTop: 20, position: 'relative', zIndex: -1, paddingBottom: 225 }}>
                                    {this.state.length == 0 ?
                                        <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: '#202020', fontWeight: 'bold', fontSize: 24, letterSpacing: 1 }}>NO BOOKINGS</Text>
                                        </View>
                                        :
                                        <View>
                                            {this.state.bookList.map((item, index) =>
                                                <TouchableOpacity onPress={this.assetDetails} onPress={() => this.bookingDetails(item.id)} style={styles.cardView}>
                                                    <View style={styles.secondCardView}>
                                                        <View style={{ width: '100%' }}>

                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Location</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                                <Text style={{ marginLeft: 10, fontSize: 14 }}>{item.location}</Text>
                                                            </View>

                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Destination</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                                <Text style={{ marginLeft: 10, fontSize: 14 }}>{item.destination}</Text>
                                                            </View>

                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Booking Date</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                                <Text style={{ marginLeft: 10, fontSize: 14 }}>{item.booking_date}</Text>
                                                            </View>

                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>End Date</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                                <Text style={{ marginLeft: 10, fontSize: 14 }}>{item.end_date}</Text>
                                                            </View>

                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Trip Status</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>:</Text>
                                                                {item.trip_status == 0 &&
                                                                    <Text style={{ marginLeft: 10, fontSize: 14 }}>Not yet start</Text>
                                                                }
                                                                {item.trip_status == 1 &&
                                                                    <Text style={{ marginLeft: 10, fontSize: 14, color: "#02bcb1" }}>Currently Running</Text>
                                                                }
                                                                {item.trip_status == 2 &&
                                                                    <Text style={{ marginLeft: 10, fontSize: 14, color: "red" }}>Closed</Text>
                                                                }
                                                            </View>

                                                            {item.trip_status == 2 &&
                                                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                                                <Text style={{ fontSize: 14, fontWeight: '900', color: '#000000', width: 110 }}>Amount</Text>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 14 }}>:</Text>
                                                                <Text style={{ marginLeft: 10, fontSize: 15 }}> {item.amount} </Text>
                                                            </View>
                                                            }
                                                        </View>
                                                    </View>

                                                </TouchableOpacity>
                                            )}
                                        </View>

                                    }
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                }
            </View>
        )
    }
}
export default TaxiBookList;
